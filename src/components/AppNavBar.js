import {NavDropdown,Nav,Navbar,Container} from 'react-bootstrap';
import {Image,Badge, Typography,Space} from 'antd';
import {useState, useEffect, useContext} from 'react';
import {NavLink, Link} from 'react-router-dom';
import PageNotFound from '../pages/PageNotFound';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Marquee from "react-fast-marquee";


// The as keyword allows components to be treated as if they are different component gaining access 
//to it's properties and functionalities
//The to keyword is used in place of the 'href' for providing the URL for the page

export default function AppNavBar() {

	const { user  } = useContext(UserContext);

	const navigate = useNavigate();

	//In here, I will capture the value from our localStorage and then store it  a state.
	//Syntax:
		// localStorage.getItem('key/property');

	// console.log(localStorage.getItem('email'));
	// const [user, setUser] = useState(localStorage.getItem('email'));

	/*const localStorage1 = localStorage.getItem('email');*/

	/*useEffect(() => {

		setUser(localStorage.getItem('email'));

		console.log("hi");

	}, [localStorage1])*/

	return (
		<Navbar  expand="lg">
		    <Container fluid>
		      <Marquee>
		        <Image
				width={150}
			 src='https://ww1.prweb.com/prfiles/2015/08/24/12921194/CapstoneLogo.jpg'
			></Image>
				</Marquee>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to = '/' >Home</Nav.Link>

		            
		            
		            	<Nav>
		            	{
		            	user.isAdmin === true 
		            	?
		            	<>
		            	    <NavDropdown
		            	      id="nav-dropdown-dark-example"
		            	      title="Admin Dashboard"
		            	      menuVariant="primary">
		            	      <NavDropdown.Item as = {NavLink} to = '/createProduct' >Add New Product</NavDropdown.Item>
		            	      <NavDropdown.Item as = {NavLink} to = '/inventory'>Product Information</NavDropdown.Item>
		            	      <NavDropdown.Item as = {NavLink} to = '/customers'>User Profile</NavDropdown.Item>
		            	      {/*<NavDropdown.Item as = {NavLink} to = '/orders'>Orders</NavDropdown.Item>*/}
		            	    </NavDropdown>
		            	          	</>
		            	:
		            	<NavDropdown.Item as = {NavLink} to = '*'></NavDropdown.Item>
		            	
		            }
		            	 </Nav>
		      
		            
		            {
		            	user.id === null || user.id ===undefined
		            	?
		            	<>
		            		<Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
		            		<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
		            	</>
		            	:
		            	<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
		            }


		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>

	)
}
