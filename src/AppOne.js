import{Space} from "antd";
import "./App.css";
import AppHeader from './components/AppHeader';
import AppFooter from './components/AppFooter';
import SideMenu from './components/SideMenu';
import PageContent from './components/PageContent';

export default function AppOne(){
	return (
		<div className="App">

			<Space className='SideMenuAndPageContent'>
				<SideMenu></SideMenu>
				<PageContent></PageContent>
			</Space>

		</div>
		);	
}