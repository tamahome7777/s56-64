import { Fragment } from 'react';
import Banner from '../components/Banner';
import Products from './Products';

export default function Home(){
	
	return (
		<Fragment>
			<Banner />
			<Products />
		</Fragment>
	)
}